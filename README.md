# vue-template

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

基于vue-cli3.0构建框架，主要功能包括
webpack 打包
axios 封装、api 设计
vuex状态管理
vue-router
Eslint
图片懒加载
css less支持
normalize
全局指令和过滤器
语言国际化
element-ui组件库
cookie
gzip
