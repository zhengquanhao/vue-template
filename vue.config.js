const path = require("path");
const autoprefixer = require("autoprefixer");
const CompressionWebpackPlugin = require("compression-webpack-plugin");

const isProduction = process.env.NODE_ENV === 'production';
const CDN = {
    css: [
        // 'https://unpkg.com/browse/element-plus@1.2.0-beta.6/theme-chalk/index.css'
    ],
    js: [
        "https://cdn.bootcdn.net/ajax/libs/vue/2.6.14/vue.min.js",
        "https://cdn.bootcdn.net/ajax/libs/vue-router/3.5.2/vue-router.min.js",
        "https://cdn.bootcdn.net/ajax/libs/vuex/3.6.2/vuex.min.js",
        'https://cdn.bootcdn.net/ajax/libs/axios/0.21.1/axios.js'
        // 'https://unpkg.com/element-plus@1.2.0-beta.6/dist/index.full.js',
        // 'https://unpkg.com/browse/element-plus@1.2.0-beta.6/lib/locale/lang/zh-cn.js'
    ]
};
let externals = {
    vue: 'Vue',
    axios: 'axios',
    vuex: 'Vuex',
    'vue-router': 'VueRouter',
    'element-plus': 'ElementPlus'
}

const resolve = dir => {
    return path.join(__dirname, dir);
}
module.exports = {
    publicPath: undefined,
    outputDir: undefined,
    assetsDir: undefined,
    runtimeCompiler: undefined,
    productionSourceMap: false,
    parallel: undefined,
    lintOnSave: true,
    devServer: {
        open: true,
        host: "0.0.0.0",
        port: 8080,
        https: false,
        // 代理相关配置，开发联调打开,
        proxy: {
            "/api": { // 自己种一下登录态
                target: "https://test.com",
                ws: true,
                secure: false,
                changeOrigin: true,
                pathRewrite: {
                    "^/api": "/"
                }
            }
        },
        disableHostCheck: true
    },
    css: {
        loaderOptions: {
            postcss: {
                plugins: [
                    autoprefixer()
                ]
            }
        }
    },
    configureWebpack: config => {
        if(isProduction) {
            // 开启gzip压缩
            const productionGzipExtensions = /\.(js|css|json|txt|html|ico|svg)(\?.*)?$/i
            config.plugins.push(
                new CompressionWebpackPlugin({
                    filename: '[path].gz[query]',
                    algorithm: 'gzip',
                    test: productionGzipExtensions,
                    threshold: 10240,
                    minRatio: 0.8
                })
            )
            // 开启分离js
            config.optimization = {
                runtimeChunk: 'single',
                splitChunks: {
                    chunks: 'all',
                    maxInitialRequests: Infinity, // 最大并行请求数，为了以防万一，设置无穷大即可
                    minSize: 20000, // 引入的模块大于20kb才做代码分割，官方默认20000，这里不用修改了
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            name (module) { // 设定分包以后的文件模块名字，按照包名字替换拼接一下
                                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]
                                return `npm.${packageName.replace('@', '')}`
                            }
                        }
                    }
                }
            };
        }
        return {
            externals: externals
        }
    },
    chainWebpack: config => {
        // 配置，将当前页定义的cdn值传到主页面（index.html）
        config.plugin('html').tap(args => {
          // 这里我是除本地环境，其余均使用CDN，可自己选择是否配置
            args[0].cdn = CDN
            return args;
        });
        config.resolve.alias
            .set("@", resolve("src"))
            .set("~", resolve("src"))
            .set("src", resolve("src"))
            .set("common", resolve("src/common"))
            .set("components", resolve("src/components"))
            .set("assets", resolve("src/assets"))
    }
}