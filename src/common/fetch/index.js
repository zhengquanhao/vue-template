import axios from "axios";
import store from "@/store";

// 添加一个响应拦截器
axios.interceptors.response.use(function (res) {
    return res;
}, function (error) {
    const data = error.response.data;
    if (data.code === 20001) {
        const currentUrl = window.location.href;
        window.location = data.data.loginUrl + "?returnUrl=" + currentUrl;
    } else {
        return Promise.reject(error);
    }
});

export default function fetch (options, header) {
    store.dispatch("setLoadingState", true);
    return new Promise((resolve, reject) => {
        const arrHeader = { "Content-Type": header || "application/json;charset=utf-8" };
        // 创建一个axios实例
        const instance = axios.create({
            // 设置默认根地址
            baseURL: process.env.VUE_APP_BASE_API,
            // 设置请求超时设置
            timeout: 30000,
            // 设置请求时的header
            headers: arrHeader,
            // 允许跨域携带cookie
            withCredentials: true
        });

        // 请求处理
        instance(options)
            .then((res) => {
                // 请求成功时,根据业务判断状态
                if (res.data) {
                    resolve(res.data);
                    store.dispatch("setLoadingState", false);
                    return false;
                } else {
                    resolve(res.data);
                    store.dispatch("setLoadingState", false);
                    return false;
                }
            })
            .catch((error) => {
                // 请求失败时,根据业务判断状态
                if (error) {
                    store.dispatch("setLoadingState", false);
                    reject(error.data);
                }
            });
    });
}
