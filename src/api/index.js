import * as apiTest from "./test";

const apiObj = {
    apiTest
};

const install = function (Vue) {
    if (install.installed) return;
    install.installed = true;

    Object.defineProperties(Vue.prototype, {
        $fetch: {
            get () {
                return apiObj;
            }
        }
    });
};

export default install;