import fetch from "../common/fetch";

const baseURL = "https://test.com"; // mock
// const baseURL = process.env.NODE_ENV === "production" ? "接口域名" : ""; // 代理

// get请求
export function getList (params) {
    return fetch({
        url: baseURL + "/about/list",
        method: "get",
        params
    });
};

// post请求
export function addList (params) {
    return fetch({
        url: "接口地址",
        method: "post",
        data : params
    });
};

// post-application形式
// export function addList (params) {
//     return fetch({
//         url: "接口地址",
//         method: "post",
//         data : params
//     }, "application/json");
// };

// restful风格接口参数拼接
export function deleteList (id, params) {
    return fetch({
        url: `接口地址/${id}`,
        method: "delete",
        params
    });
};

// 上传-文件格式
export function upload (params) {
    return fetch({
        url: baseURL + "/upload",
        method: "post",
        data: params
    }, "multipart/form-data");
}
