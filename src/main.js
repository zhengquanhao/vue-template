import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./mock/index";
import api from "./api/index";
import i18n from "./i18n";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "@/style/normalize.css";
import VueLazyLoad from "vue-lazyload";

Vue.use(VueLazyLoad, {
    preload: 1.3, // 显示当前视口高度 (window.innerHeight | 100vh) 的比例
    loading: require("~/assets/images/default.jpeg"), // 图片正在加载时显示的 loading 图片
    attempt: 3, // 下载图片时错误重连次数
    error: require("~/assets/images/default.jpeg") // 图片加载异常时显示的 error 图片
});
Vue.use(api);
Vue.use(ElementUI);
Vue.config.productionTip = false;

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount("#app");
