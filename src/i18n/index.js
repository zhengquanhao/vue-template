import Vue from "vue";
import VueI18n from "vue-i18n";
import cookie from "assets/js/cookie";
import ElementUI from "element-ui";

import elementEn from "element-ui/lib/locale/lang/en"; // element-ui 英语语言包
import elementZh from "element-ui/lib/locale/lang/zh-CN"; // element-ui 中文语言包

Vue.use(VueI18n);
Vue.use(ElementUI, {
    i18n: (key, value) => i18n.t(key, value)
});

const i18n = new VueI18n({
    locale: cookie.getCookie("language") || "zh_CN",
    messages: {
        en_US: {
            ...require("./en_US"),
            ...elementEn
        },
        zh_CN: {
            ...require("./zh_CN"),
            ...elementZh
        }
    }
});

export default i18n;