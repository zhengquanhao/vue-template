export default {
    setLoadingState (context, payload) {
        context.commit("setLoadingState", payload);
    }
};