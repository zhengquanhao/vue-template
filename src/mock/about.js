import Mock from "mockjs";

const baseUrl = "https://test.com";

Mock.mock(baseUrl + "/about/list", {
    pageName: "list"
});
Mock.mock(baseUrl + "/about/detail", {
    pageName: "detail"
});